function [e,t]=sinyal(a,b,c,d)
if(nargin<4) d=5; end
y = 0:a:b;
x=d*linspace(b, 0, length(y));
z = [y x];
w = 0:length(z)-1;
plot(w, z);

n = length(y);
f = 16;

liste1 = 100*[1:c];
liste2 = 2.^[0:d];

e=liste1(n);
t = find(liste2==f);