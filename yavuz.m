clear

load('sinyal2017hafta3.mat');
load('filtree.mat');

%% Herbir hastan�n g�r�lt� sinyali al�nd�.
sinyal1 = ecg_signals_noise(1, :);
sinyal2 = ecg_signals_noise(2, :);
sinyal3 = ecg_signals_noise(3, :);
sinyal4 = ecg_signals_noise(4, :);
sinyal5 = ecg_signals_noise(5, :);

tt = t(1:length(t)/a);
zz1 = filter(b, a, sinyal1);
zz2 = filter(b, a, sinyal2);
zz3 = filter(b, a, sinyal3);
zz4 = filter(b, a, sinyal4);
zz5 = filter(b, a, sinyal5);


%% 1. sinyal

as1=zz1(1:length(tt));   
[genlik1, rzaman1]= findpeaks(as1, t, 'MinPeakHeight', 1.8, 'MinPeakDistance', 0.2);
b1 = length(rzaman1)*6;
[~,locs_Rwave1] = findpeaks(as1(3000:4000),'MinPeakHeight',1.8,'MinPeakDistance',0.2);
ECG_inverted1 = -as1;
[~,locs_Swave1] = findpeaks(ECG_inverted1(3200:4000),'MinPeakHeight',-0.7,'MinPeakDistance',0.2);
c1 = locs_Swave1;
[~,locs_Qwave1] = findpeaks(ECG_inverted1(3500:4000),'NPeaks', 1);
k1 = locs_Qwave1;
q1 = c1-k1;
[t1, t_zaman1]=findpeaks(as1(3800:4000), 'NPeaks', 1)
[genlikT1, locsT1] = findpeaks(ECG_inverted1(3820:4880), 'MinPeakHeight', -0.70, 'MinPeakDistance', 0.2);
T1 = (t1+genlikT1)/2;
[p1, p_zaman1]=findpeaks(as1(3400:4000), 'NPeaks', 1)
[genlikP1, locsP1] = findpeaks(ECG_inverted1(3350:3400), 'NPeaks', 1);
P1 = (p1+genlikP1)/2;



%% 2.sinyal

as2=zz2(1:length(tt));   
[genlik2, rzaman2]= findpeaks(as2, t, 'MinPeakHeight',2.3, 'MinPeakDistance', 0.2);
b2 = length(rzaman2)*6;
[~,locs_Rwave2] = findpeaks(as2(3000:4000),'MinPeakHeight',2,'MinPeakDistance',0.2);
ECG_inverted2 = -as2;
[~,locs_Swave2] = findpeaks(ECG_inverted2(3650:4000),'MinPeakHeight',-0.96,'MinPeakDistance',0.2);
c2 = locs_Swave2;
[~,locs_Qwave2] = findpeaks(ECG_inverted2(3400:4000),'NPeaks', 1);
k2 = locs_Qwave2;
q2 = c2-k2;
[t2, t_zaman2]=findpeaks(as2(3800:4000), 'NPeaks', 1)
[genlikT2, locsT2] = findpeaks(ECG_inverted2(2800:2850), 'MinPeakHeight', -1, 'MinPeakDistance', 0.2);
T2 = (t2+genlikT2)/2;
[p2, p_zaman2]=findpeaks(as2(2300:2400), 'NPeaks', 1)
[genlikP2, locsP2] = findpeaks(ECG_inverted2(2250:2300), 'NPeaks', 1);
P2 = (p2+genlikP2)/2;




%% 3.sinyal

as3=zz3(1:length(tt));   
[genlik3, rzaman3]= findpeaks(as3, t, 'MinPeakHeight', 2.5, 'MinPeakDistance', 0.2);
b3 = length(rzaman3)*6;
[~,locs_Rwave3] = findpeaks(as3(3200:4000),'MinPeakHeight',2.5,'MinPeakDistance',0.2);
ECG_inverted3 = -as3;
[~,locs_Swave3] = findpeaks(ECG_inverted3(3300:4000),'MinPeakHeight',-1.5,'MinPeakDistance',0.2);
c3 = locs_Swave3;
[~,locs_Qwave3] = findpeaks(ECG_inverted3(3450:4000),'NPeaks', 1);
k3 = locs_Qwave3;
q3 = c3-k3;
[t3, t_zaman3]=findpeaks(as3(3850:4000), 'NPeaks', 1)
[genlikT3, locsT3] = findpeaks(ECG_inverted3(3950:4010), 'MinPeakHeight', -1.5, 'MinPeakDistance', 0.2);
T3 = (t3+genlikT3)/2;
[p3, p_zaman3]=findpeaks(as3(3500:4600), 'NPeaks', 1)
[genlikP3, locsP3] = findpeaks(ECG_inverted3(3410:4490), 'NPeaks', 1);
P3 = (p3+genlikP3)/2;




%% 4.sinyal

as4=zz4(1:length(tt));   
[genlik4, rzaman4]= findpeaks(as4, t, 'MinPeakHeight', 2, 'MinPeakDistance', 0.2);
b4 = length(rzaman4)*6;
[~,locs_Rwave4] = findpeaks(as4(3300:4100),'MinPeakHeight',2,'MinPeakDistance',0.2);
ECG_inverted4 = -as4;
[~,locs_Swave4] = findpeaks(ECG_inverted4(3400:4000),'MinPeakHeight',-0.1,'MinPeakDistance',0.2);
c4 = locs_Swave4;
[~,locs_Qwave4] = findpeaks(ECG_inverted4(3410:4000),'NPeaks', 1);
k4 = locs_Qwave4;
q4 = c4-k4;
[t4, t_zaman4]=findpeaks(as4(3710:4850), 'NPeaks', 1);
[genlikT4, locsT4] = findpeaks(ECG_inverted4(3800:4850), 'MinPeakHeight', -0.3, 'MinPeakDistance', 0.2);
T4 = (t4+genlikT4)/2;
[p4, p_zaman4]=findpeaks(as4(3310:4600), 'NPeaks', 1);
[genlikP4, locsP4] = findpeaks(ECG_inverted4(3250:4300), 'NPeaks', 1);
P4 = (p4+genlikP4)/2;



%% 5.sinyal

as5=zz5(1:length(tt));  
[genlik5, rzaman5]= findpeaks(as5, t, 'MinPeakHeight', 2.7, 'MinPeakDistance', 0.2);
b5 = length(rzaman5)*6;
[~,locs_Rwave5] = findpeaks(as5(3000:4000),'MinPeakHeight',2.7,'MinPeakDistance',0.2);
ECG_inverted5 = -as5;
[~,locs_Swave5] = findpeaks(ECG_inverted5(3300:4000),'MinPeakHeight',-1.8,'MinPeakDistance',0.2);
c5 = locs_Swave5;
[~,locs_Qwave5] = findpeaks(ECG_inverted5(3310:4000),'NPeaks', 1);
k5 = locs_Qwave5;
q5 = c5-k5;
[p5, p_zaman5]=findpeaks(as5(3350:4000), 'NPeaks', 1);
[genlikP5, locsP5] = findpeaks(ECG_inverted5(3360:4200), 'NPeaks', 1);
P5 = (p5+genlikP5)/2;
[t5, t_zaman5]=findpeaks(as5(2710:2850), 'NPeaks', 1);
[gT5, locsT5] = findpeaks(ECG_inverted5(3700:4750), 'MinPeakHeight', -1.8, 'MinPeakDistance', 0.2);
T5 = (t5+gT5)/2;

%% de�i�kenler olu�turuldu.
qrs_dur = [q1; q2; q3; q4; q5];
beat = [b1;b2;b3;b4;b5];
Pamp = [P1;P2;P3;P4;P5];
Tamp = [T1;T2;T3;T4;T5];