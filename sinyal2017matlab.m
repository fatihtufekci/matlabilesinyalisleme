clear
%% ecg_noise g�r�lt� sinyalini �izdirdik.
load('sinyal2017.mat');
figure(1)
plot(t, ecg_noise)
title('Gurultulu Sinyal');

%% Sinyalin spektrumu �izildi.
n = length(ecg_noise); 
y = fft(ecg_noise); % ecg_noise sinyalinin fourier transformunu ald�k.
f = (0:length(y)-1)*fs/n; % Spektrum �izmemiz i�in frekans�m�z� olu�turduk.
figure(2)
plot(f,abs(y)) % Biz y'nin imajiner taraf�yla ilgelenmedi�imiz i�in mutlak de�erini al�r�z.
               % ve spektrumunu �izeriz. Spektrumu sa�dan ve soldan �izer.
               
%% Spektrum shift edildi ve tekrar spekturumu �izildi.
yshift = fftshift(y); %% Spekturumu ortada shift ederiz.
fshift = (-n/2:n/2-1)*fs/n;
figure(3)
plot(fshift, abs(yshift));
ylabel('genlik');
xlabel('frekans');
xlim([-80 80]);
title('Genlik Spektrumu');

a = max(yshift);
fatih = find(a==yshift);
ist = find(3.2==fshift);
naber = findpeaks(abs(yshift));

%%sinyalde hangi frekanslarda bile�en vard�r?
plot(f, abs(ecg_noise))


