clear

%% Sinyalleri olusturma
f1=150;
f2=200;
fs=100000;
t=0:1/fs:5;
x = sin(2*pi*f1*t) + sin(2*pi*f2*t);
figure(1)
plot(t,x)
title('Sinyal');

%% a�a��da x sinyalinin fourier transformunu ald�k
n = length(x);
y=fft(x);
f = (0:length(y)-1)*fs/n;
figure(2)
plot(f,abs(y));

yshift = fftshift(y);
fshift = (-n/2:n/2-1)*fs/n;
figure(3)
plot(fshift,abs(yshift));

%%

%noise = 0.1*cos(2*pi*100*t);

tpartial = t(1:length(t)/200);


noise = 0.1*rand(1, length(t));
xnoise = x + noise;
figure(4)
plot(tpartial,x(1:length(tpartial)))
hold on
plot(tpartial,xnoise(1:length(tpartial)))



%% filtre
figure(5)
plot(tpartial,x(1:length(tpartial)))
legend('x')
hold on
plot(tpartial,xnoise(1:length(tpartial)), 'DisplayName', 'xnoise');



point = 30;
b = 1/point * ones(1,point);
y1 = filter(b, 1, xnoise);



plot(tpartial,y1(1:length(tpartial)), 'DisplayName', num2str(point));
point


%%