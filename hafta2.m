clear

%%ecg_noise sinyali çizildi
load('filtre.mat');
load('sinyal2017hafta2.mat');
figure(1)
plot(t, ecg_noise)
title('ecg_noise gürültü sinyali');

%% Sinyalin spektrumu çizildi.
n = length(ecg_noise); 
y = fft(ecg_noise); % ecg_noise sinyalinin fourier transformunu aldık.
yshift = fftshift(y); %% Spekturumu ortada shift ederiz.
fshift = (-n/2:n/2-1)*fs/n;
% Biz sinyalin imajiner tarafıyla ilgilenmediğimiz için mutlak değerini
% çizdiririz.
figure(2)
plot(fshift, abs(yshift)); 
ylabel('genlik');
xlabel('frekans');
xlim([-80 80]);
title('Genlik Spektrumu');

%% Gürültü sinyalinin filtreden geçirilmesi

tpartial = t(1:length(t)/den);

% filte.mat tan aldığımız değerler ile gürültülü sinyali filtreden geçirdik.
z = filter(num, den, ecg_noise);      
figure(3)
plot(t, ecg_noise)                    % Gürültülü sinyal
hold on
plot(tpartial,z(1:length(tpartial)))  % Filtreden geçmiş sinyal
title('ecg_noise gürültü sinyali ile gürültüden arındırılmış sinyalin karşılaştırılması');
xlim([0 10]);


%% Filtreli sinyalin spektrumunun çizdirilmesi

n2 = length(z);
y1 = fft(z);
y1shift = fftshift(y1); %% Spekturumu ortada shift ederiz.
fy1shift = (-n2/2:n2/2-1)*fs/n2;
figure(4)
plot(fshift, abs(yshift));
hold on
plot(fy1shift, abs(y1shift));
ylabel('genlik');
xlabel('frekans');
xlim([-80 80]);
title('gürültü sinyali ile filtreli sinyalin spektrumlarının karşılaştırılması');


%% Filtreli sinyalin peak(R) noktalarının bulunup çizdirilmesi

% Filtreli sinyalimizi başka bir sinyal vektörüne atadık. Daha sonta
% findpeaks metodu ile tepe noktaları bulucaz.
gg=z(1:length(tpartial));   
figure(5)
findpeaks(gg, fs, 'MinPeakHeight', 0.9) % Tepe noktaların bulunması
title('filtreli sinyalin peak noktaları');

findpeaks(gg);

%% R tepelerinin zamanlarının bulunması

%rzaman = [0.853 1.686 2.521 3.353 4.186 5.021 6.686 7.521 8.353 9.186];
[genlik, rzaman]= findpeaks(gg, t, 'MinPeakHeight', 0.5, 'MinPeakDistance', 0.2);

%% R tepeleri arasındaki zaman farklarının bulunması.
% Zaman farklarını koyacağımız vektörü oluşturup içini 0 ile dolduruyoruz.
% Ve zaman farkları rzaman vektörünün bir eksiğini vereceğinden vektörün
% boyutunu ona uygun ayarlarız.
rzamanfark = zeros(1, length(rzaman)-1);
c = rzaman(1);
for i = 2:length(rzaman)
   rzamanfark(i-1) = rzaman(i) - c;
   c = rzaman(i);
end


%% R tepelerinin arasındaki zaman farklarının ortalamasının bulunması
sum = 0;
for j=1:length(rzamanfark)
    sum = sum + rzamanfark(j);
end

% rzamanfark vektörünün elemanlarını toplayıp, uzunluğuna böldük ve
% ortalama zaman farkını bulduk.
ort_R_araligi = sum / length(rzamanfark);

