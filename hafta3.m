clear

load('sinyal2017hafta3.mat');
load('filtre2.mat');

%% Herbir hastan�n g�r�lt� sinyali al�nd�.
birinci_hasta = ecg_signals_noise(1, :);
ikinci_hasta = ecg_signals_noise(2, :);
ucuncu_hasta = ecg_signals_noise(3, :);
dorduncu_hasta = ecg_signals_noise(4, :);
besinci_hasta = ecg_signals_noise(5, :);


%% 1. hastan�n sinyalin spektrumu..
n = length(birinci_hasta); 
y = fft(birinci_hasta); % ecg_noise sinyalinin fourier transformunu ald�k.
yshift = fftshift(y); %% Spekturumu ortada shift ederiz.
fshift = (-n/2:n/2-1)*fs/n;

%% 40Hz den b�y�k frekansl� g�r�lt� sinyalini bulma 

c = abs(yshift);
[p,l]=gurultuSinyaliniBul(c);

%% Filtre zaman�
tpartial = t(1:length(t)/den);

%% Birinci sinyalin filtreden ge�irilmesi
z1 = filtfilt(num, den, birinci_hasta);

%% �kinci sinyalin filtreden ge�irilmesi
z2 = filtfilt(num, den, ikinci_hasta);

%% ���nc� sinyalin filtreden ge�irilmesi
z3 = filtfilt(num, den, ucuncu_hasta);

%% D�rd�nc� sinyalin filtreden ge�irilmesi
z4 = filtfilt(num, den, dorduncu_hasta);

%% Besinci sinyalin filtreden ge�irilmesi
z5 = filtfilt(num, den, besinci_hasta);


%% 1. hastan�n sinyalinin tepe noktalar�n�n bulunmas�

gg1=z1(1:length(tpartial));   

%% 1. hastan�n sinyalinin Dakikadaki kalp at�� say�s�n�n bulunmas�

[genlik1, rzaman1]= findpeaks(gg1, t, 'MinPeakHeight', 2.2, 'MinPeakDistance', 0.2);
beat1 = length(rzaman1)*6;


%% 1. hastan�n sinyalinin Q-R-S
[~,locs_Rwave1] = findpeaks(gg1(2000:3000),'MinPeakHeight',2.2,'MinPeakDistance',0.2);

% S wave
ECG_inverted1 = -gg1;
%findpeaks(ECG_inverted(2400:3000),'MinPeakHeight',-0.8,'MinPeakDistance',0.2);
[~,locs_Swave1] = findpeaks(ECG_inverted1(2400:3000),'MinPeakHeight',-0.8,'MinPeakDistance',0.2);

%t s�resini verdi�imizde 0-10 aras� grafi�i �izerken, parametrede t yi
%vermedi�imizden ayn� grafi�i 0-10000 aras� �iziyor.Bunun i�in q ve s i
%1000 e b�ld�k
s1 = locs_Swave1/1000;

% Q wave
[~,locs_Qwave1] = findpeaks(ECG_inverted1(2400:3000),'NPeaks', 1);
q1 = locs_Qwave1/1000;

qrs_dur1 = s1-q1;

%% 1. hastan�n sinyalinin T genli�i
%findpeaks(gg1(2000:3000),'MinPeakHeight', 2.2);
[t_genligi1, t_zaman1]=findpeaks(gg1(2700:3000), 'NPeaks', 1)
[genlikT1, locsT1] = findpeaks(ECG_inverted1(2800:2850), 'MinPeakHeight', -0.95, 'MinPeakDistance', 0.2);
Tamp1 = (t_genligi1+genlikT1)/2;

%% 1. hastan�n sinyalinin P genli�i
[p_genligi1, p_zaman1]=findpeaks(gg1(2300:3000), 'NPeaks', 1)
[genlikP1, locsP1] = findpeaks(ECG_inverted1(2250:2300), 'NPeaks', 1);
Pamp1 = (p_genligi1+genlikP1)/2;






%% 2.hastan�n sinyalinin tepe noktalar�n�n bulunmas�

gg2=z2(1:length(tpartial));   

%% 2. hastan�n sinyalinin Dakikadaki kalp at�� say�s�n�n bulunmas�

[genlik2, rzaman2]= findpeaks(gg2, t, 'MinPeakHeight', 2.2, 'MinPeakDistance', 0.2);
beat2 = length(rzaman2)*6;


%% 2. hastan�n sinyalinin Q-R-S
[~,locs_Rwave2] = findpeaks(gg2(2000:3000),'MinPeakHeight',2.2,'MinPeakDistance',0.2);

% S wave
ECG_inverted2 = -gg2;
%findpeaks(ECG_inverted2(2400:3000),'MinPeakHeight',-0.8,'MinPeakDistance',0.2);
[~,locs_Swave2] = findpeaks(ECG_inverted2(2550:3000),'MinPeakHeight',-0.8,'MinPeakDistance',0.2);

%t s�resini verdi�imizde 0-10 aras� grafi�i �izerken, parametrede t yi
%vermedi�imizden ayn� grafi�i 0-10000 aras� �iziyor.Bunun i�in q ve s i
%1000 e b�ld�k.
s2 = locs_Swave2/1000;

% Q wave
[~,locs_Qwave2] = findpeaks(ECG_inverted2(2380:3000),'NPeaks', 1);
q2 = locs_Qwave2/1000;

qrs_dur2 = s2-q2;

%% 2. hastan�n sinyalinin T genli�i
%findpeaks(gg2(2000:3000),'MinPeakHeight', 2.2);
[t_genligi2, t_zaman2]=findpeaks(gg2(2700:2800), 'NPeaks', 1)
[genlikT2, locsT2] = findpeaks(ECG_inverted2(2800:2850), 'MinPeakHeight', -1, 'MinPeakDistance', 0.2);
Tamp2 = (t_genligi2+genlikT2)/2;

%% 2. hastan�n sinyalinin P genli�i
[p_genligi2, p_zaman2]=findpeaks(gg2(2300:2400), 'NPeaks', 1)
[genlikP2, locsP2] = findpeaks(ECG_inverted2(2250:2300), 'NPeaks', 1);
Pamp2 = (p_genligi2+genlikP2)/2;







%% 3.hastan�n sinyalinin tepe noktalar�n�n bulunmas�

gg3=z3(1:length(tpartial));   

%% 3. hastan�n sinyalinin Dakikadaki kalp at�� say�s�n�n bulunmas�

[genlik3, rzaman3]= findpeaks(gg3, t, 'MinPeakHeight', 2.2, 'MinPeakDistance', 0.2);
beat3 = length(rzaman3)*6;

%% 3. hastan�n sinyalinin Q-R-S
[~,locs_Rwave3] = findpeaks(gg3(2300:3100),'MinPeakHeight',2.2,'MinPeakDistance',0.2);

% S wave
ECG_inverted3 = -gg3;
%findpeaks(ECG_inverted3(2400:3000),'MinPeakHeight',-1.2,'MinPeakDistance',0.2);
[~,locs_Swave3] = findpeaks(ECG_inverted3(2400:3000),'MinPeakHeight',-1.2,'MinPeakDistance',0.2);

%t s�resini verdi�imizde 0-10 aras� grafi�i �izerken, parametrede t yi
%vermedi�imizden ayn� grafi�i 0-10000 aras� �iziyor.Bunun i�in q ve s i
%1000 e b�ld�k
s3 = locs_Swave3/1000;

% Q wave
[~,locs_Qwave3] = findpeaks(ECG_inverted3(2550:3000),'NPeaks', 1);
q3 = locs_Qwave3/1000;

qrs_dur3 = s3-q3;


%% 3. hastan�n sinyalinin T genli�i
%findpeaks(gg3(2300:3100),'MinPeakHeight', 2.2);
[t_genligi3, t_zaman3]=findpeaks(gg3(2900:2950), 'NPeaks', 1)
[genlikT3, locsT3] = findpeaks(ECG_inverted3(2970:3010), 'MinPeakHeight', -1.34, 'MinPeakDistance', 0.2);
Tamp3 = (t_genligi3+genlikT3)/2;

%% 3. hastan�n sinyalinin P genli�i
[p_genligi3, p_zaman3]=findpeaks(gg3(2500:2600), 'NPeaks', 1)
[genlikP3, locsP3] = findpeaks(ECG_inverted3(2410:2490), 'NPeaks', 1);
Pamp3 = (p_genligi3+genlikP3)/2;








%% 4.hastan�n sinyalinin tepe noktalar�n�n bulunmas�

gg4=z4(1:length(tpartial));   

%% 4. hastan�n sinyalinin Dakikadaki kalp at�� say�s�n�n bulunmas�

[genlik4, rzaman4]= findpeaks(gg4, t, 'MinPeakHeight', 1.8, 'MinPeakDistance', 0.2);
beat4 = length(rzaman4)*6;

%% 4. hastan�n sinyalinin Q-R-S
[~,locs_Rwave4] = findpeaks(gg4(2300:3100),'MinPeakHeight',1.8,'MinPeakDistance',0.2);

% S wave
ECG_inverted4 = -gg4;
%findpeaks(ECG_inverted4(2400:3000),'MinPeakHeight',-0.5,'MinPeakDistance',0.2);
[~,locs_Swave4] = findpeaks(ECG_inverted4(2400:3000),'MinPeakHeight',-0.5,'MinPeakDistance',0.2);

%t s�resini verdi�imizde 0-10 aras� grafi�i �izerken, parametrede t yi
%vermedi�imizden ayn� grafi�i 0-10000 aras� �iziyor.Bunun i�in q ve s i
%1000 e b�ld�k
s4 = locs_Swave4/1000;

% Q wave
[~,locs_Qwave4] = findpeaks(ECG_inverted4(2410:3000),'NPeaks', 1);
q4 = locs_Qwave4/1000;

qrs_dur4 = s4-q4;

%% 4. hastan�n sinyalinin T genli�i
%findpeaks(gg4(2000:3000),'MinPeakHeight', 1.8);
[t_genligi4, t_zaman4]=findpeaks(gg4(2710:2850), 'NPeaks', 1);
[genlikT4, locsT4] = findpeaks(ECG_inverted4(2800:2850), 'MinPeakHeight', -0.59, 'MinPeakDistance', 0.2);
Tamp4 = (t_genligi4+genlikT4)/2;

%% 4. hastan�n sinyalinin P genli�i
[p_genligi4, p_zaman4]=findpeaks(gg4(2310:2600), 'NPeaks', 1);
[genlikP4, locsP4] = findpeaks(ECG_inverted4(2250:2300), 'NPeaks', 1);
Pamp4 = (p_genligi4+genlikP4)/2;










%% 5.hastan�n sinyalinin tepe noktalar�n�n bulunmas�

gg5=z5(1:length(tpartial));   

%% 5. hastan�n sinyalinin Dakikadaki kalp at�� say�s�n�n bulunmas�

[genlik5, rzaman5]= findpeaks(gg5, t, 'MinPeakHeight', 2.8, 'MinPeakDistance', 0.2);
beat5 = length(rzaman5)*6;


%% 5. hastan�n sinyalinin Q-R-S
[~,locs_Rwave5] = findpeaks(gg5(2300:3100),'MinPeakHeight',2.8,'MinPeakDistance',0.2);

% S wave
ECG_inverted5 = -gg5;
%findpeaks(ECG_inverted5(2400:3000),'MinPeakHeight',-1.4,'MinPeakDistance',0.2);
[~,locs_Swave5] = findpeaks(ECG_inverted5(2400:3000),'MinPeakHeight',-1.4,'MinPeakDistance',0.2);


%t s�resini verdi�imizde 0-10 aras� grafi�i �izerken, parametrede t yi
%vermedi�imizden ayn� grafi�i 0-10000 aras� �iziyor.Bunun i�in q ve s i
%1000 e b�ld�k
s5 = locs_Swave5/1000;

% Q wave
[~,locs_Qwave5] = findpeaks(ECG_inverted5(2410:3000),'NPeaks', 1);
q5 = locs_Qwave5/1000;

qrs_dur5 = s5-q5;

%% 5. hastan�n sinyalinin P genli�i
[p_genligi5, p_zaman5]=findpeaks(gg5(2250:2600), 'NPeaks', 1);
[genlikP5, locsP5] = findpeaks(ECG_inverted5(2260:2300), 'NPeaks', 1);
%findpeaks(ECG_inverted5(2350:2415), 'MinPeakHeight', -1.55, 'MinPeakDistance', 0.2);
Pamp5 = (p_genligi5+genlikP5)/2;

%% 5. hastan�n sinyalinin T genli�i
%findpeaks(gg5(2000:3000),'MinPeakHeight', 2.8);
[t_genligi5, t_zaman5]=findpeaks(gg5(2710:2850), 'NPeaks', 1);
[genlikT5, locsT5] = findpeaks(ECG_inverted5(2700:2850), 'MinPeakHeight', -1.5, 'MinPeakDistance', 0.2);
Tamp5 = (t_genligi5+genlikT5)/2;





%% Matrislerin olu�turulmas�
qrs_dur = [qrs_dur1; qrs_dur2; qrs_dur3; qrs_dur4; qrs_dur5];
beat = [beat1;beat2;beat3;beat4;beat5];
Pamp = [Pamp1;Pamp2;Pamp3;Pamp4;Pamp5];
Tamp = [Tamp1;Tamp2;Tamp3;Tamp4;Tamp5];


%% Hastal�k te�hisleri
teshisler = [0; 0; 0; 0; 0];
hastalik = strings(5,1);

for i=1:5
    if Pamp(i)>0.3
        teshisler(i)=1;
        hastalik(i)="Koroner Arter Hastal���";
    elseif beat(i)>80
        teshisler(i)=2;
        hastalik(i)="Mitral Kapak Hastal���";
    elseif Tamp(i)<0.25
        teshisler(i)=3;
        hastalik(i)="Aort Kapak Hastal���"; 
    elseif qrs_dur(i)<0.08
        teshisler(i)=4;
        hastalik(i)="Aort Anevrimas�";
    elseif beat(i)<60
        teshisler(i)=5;
        hastalik(i)="Atriyal Septal Defekt";
    elseif Pamp(i)<0.1
        teshisler(i)=6;
        hastalik(i)="Kalpte Delik";
    elseif qrs_dur(i)>0.15
        teshisler(i)=7;
        hastalik(i)="Kalp Yetmezli�i";
    elseif Tamp(i)>0.4 
        teshisler(i)=8;
        hastalik(i)="Miyoptakiler";
    else
        teshisler(i)=9;
        hastalik(i)="Sa�lam";
    end
end

%% 1. hasta grafikleri
figure(1)
subplot(2,2,1);
plot(t, birinci_hasta)
title('1.hastan�n g�r�lt� sinyali');

subplot(2,2,2);
plot(tpartial,z1(1:length(tpartial)))
title('1.hastan�n filtreli sinyalinin spektrumu');

n1 = length(z1);
y1 = fft(z1);
y1shift = fftshift(y1); %% Spekturumu ortada shift ederiz.
fy1shift = (-n1/2:n1/2-1)*fs/n1;
subplot(2,2,[3,4]);
plot(fy1shift, abs(y1shift));
xlim([-15 15]);
title(['qrs_dur:', qrs_dur(1), ' beat:', beat(1), ' Tgenligi:', Tamp(1), ' Pgenligi:', Pamp(1), 'Teshis: '...
    , teshisler(1), '-',hastalik(1)])


%% 2. hasta grafikleri
figure(2)
subplot(2,2,1);
plot(t, ikinci_hasta)
title('2.hastan�n g�r�lt� sinyali');

subplot(2,2,2);
plot(tpartial,z2(1:length(tpartial)))
title('2.hastan�n filtreli sinyalinin spektrumu');

n2 = length(z2);
y2 = fft(z2);
y2shift = fftshift(y2); %% Spekturumu ortada shift ederiz.
fy2shift = (-n2/2:n2/2-1)*fs/n2;
subplot(2,2,[3,4]);
plot(fy2shift, abs(y2shift));
xlim([-15 15]);
title(['qrs_dur:', qrs_dur(2), ' beat:', beat(2), ' Tgenligi:', Tamp(2), ' Pgenligi:', Pamp(2), 'Teshis: '...
    , teshisler(2), '-',hastalik(2)])


%% 3. hasta grafikleri
figure(3)
subplot(2,2,1);
plot(t, ucuncu_hasta)
title('3.hastan�n g�r�lt� sinyali');

subplot(2,2,2);
plot(tpartial,z3(1:length(tpartial)))
title('3.hastan�n filtreli sinyalinin spektrumu');

n3 = length(z3);
y3 = fft(z3);
y3shift = fftshift(y3); %% Spekturumu ortada shift ederiz.
fy3shift = (-n3/2:n3/2-1)*fs/n3;
subplot(2,2,[3,4]);
plot(fy3shift, abs(y3shift));
xlim([-15 15]);
title(['qrs_dur:', qrs_dur(3), ' beat:', beat(3), ' Tgenligi:', Tamp(3), ' Pgenligi:', Pamp(3), 'Teshis: '...
    , teshisler(3), '-',hastalik(3)])

%% 4. hasta grafikleri
figure(4)
subplot(2,2,1);
plot(t, dorduncu_hasta)
title('4.hastan�n g�r�lt� sinyali');

subplot(2,2,2);
plot(tpartial,z4(1:length(tpartial)))
title('4.hastan�n filtreli sinyalinin spektrumu');

n4 = length(z4);
y4 = fft(z4);
y4shift = fftshift(y4); %% Spekturumu ortada shift ederiz.
fy4shift = (-n4/2:n4/2-1)*fs/n4;
subplot(2,2,[3,4]);
plot(fy4shift, abs(y4shift));
xlim([-15 15]);
title(['qrs_dur:', qrs_dur(4), ' beat:', beat(4), ' Tgenligi:', Tamp(4), ' Pgenligi:', Pamp(4), 'Teshis: '...
    , teshisler(4), '-',hastalik(4)])

%% 5. hasta grafikleri
figure(5)
subplot(2,2,1);
plot(t, besinci_hasta)
title('5.hastan�n g�r�lt� sinyali');

subplot(2,2,2);
plot(tpartial,z5(1:length(tpartial)))
title('5.hastan�n filtreli sinyalinin spektrumu');

n5 = length(z5);
y5 = fft(z5);
y5shift = fftshift(y5); %% Spekturumu ortada shift ederiz.
fy5shift = (-n5/2:n5/2-1)*fs/n5;
subplot(2,2,[3,4]);
plot(fy5shift, abs(y5shift));
xlim([-15 15]);
title(['qrs_dur:', qrs_dur(5), ' beat:', beat(5), ' Tgenligi:', Tamp(5), ' Pgenligi:', Pamp(5), 'Teshis: '...
    , teshisler(5), '-',hastalik(5)])