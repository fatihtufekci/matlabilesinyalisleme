function [p, l]=gurultuSinyaliniBul(c)

% Burada frekans� shift etmedi�imiz i�in 40Hz i 5400Hz olarak kabul ettik.
% Ve genli�i 200 den fazla olan sinyali bulduk.
[p,l]=findpeaks(c(5400:length(c)),'MinPeakHeight', 200);

end
